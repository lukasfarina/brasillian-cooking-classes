var mousestopedTimer;
var interval;
var socialInterval;

(function ($) {

    /*
    * On document Ready
    * */
    $(function () {

        var md = new MobileDetect(window.navigator.userAgent);

        var currentPlayer,
            vid = document.getElementById("teaser"),
            freeClassPlayer = document.getElementById('freeclass-video'),

            htmlTag = document.getElementsByTagName('html')[0],
            animatedElements = [],
            freeClass =  $('#freeclass-video'),
            modalPlayer = $('#modalPlayer'),
            modalPlayerActivator = $('.class-item .class-thumbnail'),
            skipIntro = $('#free-class-wrap').find('.skip-intro');

        htmlTag.classList.add('returning');


        // Populate animated Elements
        animatedElements = animatedElements.concat($('.section-header').get());
        animatedElements = animatedElements.concat($('.title--border-bottom').get());

        // Restart video
        vid.currentTime = 5;
        vid.play();

        // Start Carousel
        var $carouselObj = $('.owl-carousel');

        if(md.mobile() == null){
            $carouselObj.on('initialized.owl.carousel translated.owl.carousel', function (event) {

                var items = $(this).find('.owl-item.active');

                // RESET
                if(event.type == 'translated') {
                    $(this).find('.owl-item').removeClass('at-left center at-right');
                }

                for(var item in items){

                    item = parseInt(item);

                    var _current = $(items[item]),
                        _next = $(items[item + 1]);

                    // First Item
                    if(item == "0") {
                        _current.addClass('at-left');
                    }

                    if(item == "1"){
                        _current.addClass('center');
                    }

                    // Last Element
                    if(item == "2") {
                        //_current.css('opacity', '.3');
                        _current.addClass('at-right');
                    }
                }

            });

            $carouselObj.owlCarousel({
                items: 3,
                loop: true,
                nav: true,
                navText: false,

                responsive: {
                    0: {
                        items: 1
                    },

                    768:{
                        items: 3
                    }
                }
            });
        }

        $(document).on('click', '.class-item', function (e) {

            var $this = $(this);

            /*if(md.mobile() != null)
                return false;*/

            if($this.parent().hasClass('at-left')){
                $carouselObj.trigger('prev.owl.carousel');
            }

            if($this.parent().hasClass('at-right')){
                $carouselObj.trigger('next.owl.carousel');
            }
        });

        $(document).on('click', '.class-item .class-thumbnail',function (e) {

            var _parent = md.mobile() != null ? $(this).closest('.class-item') : $(this).closest('.owl-item');

            if(_parent.hasClass('center') || _parent.hasClass('class-item')){

                // Pega o src
                var mp4NewSrc = $(this).data('vid-mp4'),
                    webm = $(this).data('vid-webm');

                if(!currentPlayer) {
                    var mp4Source = modalPlayer.find('[type="video/mp4"]');
                    mp4Source.attr('src', mp4NewSrc);

                    var webmSource = modalPlayer.find('[type="video/webm"]');
                    webmSource.attr('src', webm);

                    currentPlayer = videojs('modalVideo');
                }

                else {
                    currentPlayer.src([{type: "video/webm", src: webm}, { type: "video/mp4", src: mp4NewSrc }]);
                }

                // Load & Play
                currentPlayer.load();
                currentPlayer.play();

                if(md.mobile() != null) {
                    currentPlayer.requestFullscreen();
                }

                // Set button
                modalPlayer.find('.see-all-button').attr('href', _parent.find('.class-buy-action').attr('href'));

                // Show
                modalPlayer.modal();

                // When Video Has FInished
                currentPlayer.ready(function (e) {
                    this.on('ended', function (e) {
                        modalPlayer.addClass('finish');
                        currentPlayer.exitFullscreen();
                    });
                });

            }
            else {
                if(_parent.hasClass('at-left')){
                    $carouselObj.trigger('prev.owl.carousel');
                }

                if(_parent.hasClass('at-right')){
                    $carouselObj.trigger('next.owl.carousel');
                }
            }

        });

        modalPlayer.on('hide.bs.modal', function (e) {
           currentPlayer.pause();

            $(this).removeClass('finish');
        });

        freeClass.on('ended', function (e) {
            $('#free-class-wrap').addClass('finish');
        });

        if(md.mobile() == 'iPhone'){
            freeClass.addClass('iphone');

            $('#free-class-wrap').addClass('iphonePlayer');

            freeClass.attr('webkit-playsinline', true);
            freeClass.attr('playsinline', true);
        }else{

            freeClass.addClass('video-js vjs-default-skin free-class-video');

            freeClass.attr('data-setup', true);

            // Add Skip Intro to Free Class Video
            videojs("freeclass-video").ready(function () {
                var player = this;

                skipIntro.appendTo('#freeclass-video');
            });
        }

        // Add Action to Skip
        skipIntro.click(function (e) {
            freeClassPlayer.currentTime = 94;
        });


        freeClass.on('timeupdate', function(){
//          //console.log('the time was updated to: ' + this.currentTime);

            if(this.currentTime > 10 && this.currentTime < 94){
                skipIntro.css('opacity', 1);
            }
            else {
                skipIntro.css('opacity', 0);
            }

        });

        $('#scroll-down').on('click', function (e) {
            $('html, body').animate({
                scrollTop: $("#see-all").offset().top
            }, 1000);
        });

        $('.share-in')
            .on('mouseover', function (e) {

                var _parent = $(this).closest('.owl-item');

                var $this = $(this),
                    currentPostion = $(this).position(),
                    $socialOptions = $('.share-options');

                $socialOptions.appendTo($this);

                // Prepare
                $socialOptions
                    .css({/*'top': currentPostion.top, 'left': currentPostion.left, */'display': 'block'});

                // Timer for want true
                setTimeout(function () {

                    // If want true
                    $socialOptions.addClass('active');

                }, 500);

            })
            .on('mouseout', function (e) {

                var $socialOptions = $('.share-options'),
                    $this = $(this);

                setTimeout(function () {

                    if($this.is(':hover') || $socialOptions.is(':hover')){
                        return true;
                    }

                    $socialOptions.removeClass('active');

                    setTimeout(function () {
                        $socialOptions.css('display', 'none');
                    }, 200)

                }, 1000);

            });


        // Waypoints
        $(animatedElements).each(function (key, element) {
            $(element).waypoint({ handler: function () {
                $(element).addClass('onscreen');
            },  offset: '75%'});
        });

    });

})(jQuery);

function delay (elem, callback) {
    var timeout = null;
    elem.onmouseover = function() {
        // Set timeout to be a timer which will invoke callback after 1s
        timeout = setTimeout(callback, 1000);
    };

    elem.onmouseout = function() {
        // Clear any timers set to timeout
        clearTimeout(timeout);
    }
}

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}