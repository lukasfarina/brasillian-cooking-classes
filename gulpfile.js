var gulp =      require('gulp');
var sass =      require('gulp-sass');
var merge =     require('merge-stream');
var rename =    require('gulp-rename');
var concat =    require('gulp-concat');
var minifycss = require('gulp-minify-css');
var watch =     require('gulp-watch');
var minifyjs =  require('gulp-uglify');

// CSS
gulp.task('styles', function() {
    var sassStream, cssStream, cssMin;

    //compile sass
    sassStream = gulp.src('assets/css/sass/styles.scss').pipe(sass({
        errLogToConsole: true
    }));

    //select additional css files
    cssStream = gulp.src([
        'assets/css/lib/bootstrap.min.css',
        'assets/css/lib/animate.css',
        'assets/css/lib/owl.carousel.css',
        'assets/css/lib/owl.carousel.default.css',
        'bower_components/video.js/dist/video-js.min.css'
    ]);

    // Merge Streams
    cssMin = merge(sassStream, cssStream)
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('assets/css/'))
        .pipe(rename('styles.min.css'))
        .pipe(minifycss())
        .pipe(gulp.dest('assets/css/'));
});

// Scripts
gulp.task('scripts', function () {
    gulp.src([
            'assets/js/lib/jquery-1.11.2.min.js',
            'assets/js/lib/bootstrap.min.js',
            'assets/js/lib/owl.carousel.min.js',
            'bower_components/video.js/dist/video.min.js',

            //'bower_components/iphone-inline-video/dist/iphone-inline-video.browser.js',

            'bower_components/mobile-detect/mobile-detect.min.js',
            'bower_components/waypoints/lib/jquery.waypoints.js',
            'assets/js/lib/main.js'
    ])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('assets/js/'))
        .pipe(rename('scripts.min.js'))
        .pipe(minifyjs())
        .pipe(gulp.dest('assets/js/'));

});

// Observer
gulp.task('watch',function() {

    gulp.watch([
        'assets/css/lib/*.css',
        'assets/css/sass/*.scss'
    ],['styles']);

    gulp.watch('assets/js/lib/**.js', ['scripts']);
});

// Default
gulp.task('default',['styles','scripts','watch']);